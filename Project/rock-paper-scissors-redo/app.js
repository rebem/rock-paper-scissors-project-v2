let playerChosen;
let computerChosen;
var result = results();
const displayResult = document.getElementById("results");
const computerChoice = document.getElementById("computer-choice");
const randomNumber = Math.round(Math.random() * 3);
const playerChoice = document.getElementById("player-choice");
const possibleChoices = document.querySelectorAll(".choices");

possibleChoices.forEach((possibleChoice) =>
  possibleChoice.addEventListener("click", (e) => {
    playerChosen = e.target.id;
    generatedComputerChoice();
    results();
    playerChoice.innerHTML = playerChosen;
    computerChoice.innerHTML = computerChosen;
    displayResult.innerHTML = result;
  })
);

function generatedComputerChoice() {
  if (randomNumber === 1) {
    return (computerChosen = "rock");
  } else if (randomNumber === 2) {
    return (computerChosen = "paper");
  } else if (randomNumber === 3) {
    return (computerChosen = "scissors");
  }
}
function results() {
  if (computerChosen === playerChosen) {
    return (results = "It is a tie");
  } else if (computerChosen === "rock" && playerChosen === "paper") {
    return (results = "You won");
  } else if (computerChosen === "rock" && playerChosen === "scissors") {
    return (results = "You lost");
  } else if (computerChosen === "paper" && playerChosen === "rock") {
    return (results = "You lost");
  } else if (computerChosen === "paper" && playerChosen === "scissors") {
    return (results = "You won");
  } else if (computerChosen === "scissors" && playerChosen === "paper") {
    return (results = "You won");
  } else if (computerChosen === "scissors" && playerChosen === "rock") {
    return (results = "You lost");
  }
}
